<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ctype extends Model
{
    use HasFactory;
    public function classrooms()
    {
        return $this->hasMany(Classroom::class);
    }
}
