<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCtypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //type de salle
    public function up()
    {
        Schema::create('ctypes', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctypes');
    }
}
